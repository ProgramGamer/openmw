#include <filesystem>
#include <iostream>

#include <boost/program_options.hpp>

#include <components/files/configurationmanager.hpp>

#include "importer.hpp"

namespace fs = std::filesystem;
namespace bpo = boost::program_options;

static const fs::path omwExt = ".omwsave";

int main(int argc, char** argv)
{
    try
    {
        // Describe option arguments
        bpo::options_description desc("(Syntax: openmw-essimporter <options> infile.ess outfile.omwsave\nAllowed options)");
        auto addOption = desc.add_options();
        addOption("help,h", "produce help message");
        addOption("mwsave,m", bpo::value<Files::MaybeQuotedPath>(), "morrowind .ess save file");
        addOption("output,o", bpo::value<Files::MaybeQuotedPath>(), "output file (.omwsave)");
        addOption("compare,c", "compare two .ess files");
        addOption("encoding", bpo::value<std::string>()->default_value("win1252"), "encoding of the save file");
        Files::ConfigurationManager::addCommonOptions(desc);

        // Describe positional arguments
        bpo::positional_options_description p_desc;
        p_desc.add("mwsave", 1).add("output", 1);

        // Parse arguments and store into variables
        bpo::command_line_parser parser(argc, argv);
        bpo::parsed_options parsed = parser.options(desc).positional(p_desc).run();
        bpo::variables_map variables;
        bpo::store(parsed, variables);

        // Print description and return
        if (variables.count("help") || !variables.count("mwsave") || !variables.count("output"))
        {
            std::cout << desc;
            return 0;
        }

        // Post-process variables
        bpo::notify(variables);
        Files::ConfigurationManager cfgManager(true);
        cfgManager.readConfiguration(variables, desc);

        // Get input/output paths
        const auto essFile = variables["mwsave"].as<Files::MaybeQuotedPath>();
        const auto outputFile = variables["output"].as<Files::MaybeQuotedPath>();
        const auto encoding = variables["encoding"].as<std::string>();

        // Create and use importer
        ESSImport::Importer importer(essFile, outputFile, encoding);

        if (variables.count("compare"))
        {
            importer.compare();
        }
        else
        {
            if (fs::exists(outputFile) && outputFile.has_extension() && outputFile.extension() != omwExt)
            {
                throw std::runtime_error("Output file already exists and does not end in .omwsave. Did you mean to use --compare?");
            }
            else
            {
                importer.run();
            }
        }
    }
    catch (std::exception& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}
