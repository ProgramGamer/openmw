#include "importjour.hpp"

#include <string_view>
#include <iostream>
#include <charconv>
#include <set>
#include <map>

#include <components/esm3/esmreader.hpp>

namespace ESSImport
{

    static const std::set<char> whitespace = { ' ', '\n', '\r' };

    // TODO: move this into a utility function somewhere else and account for other localizations
    static const std::map<std::string, int, std::less<void>> months
    {
        { "Morning Star", 0 },
        { "Sun's Dawn", 1 },
        { "First Seed", 2 },
        { "Rain's Hand", 3 },
        { "Second Seed", 4 },
        { "Mid Year", 5 },
        { "Sun's Height", 6 },
        { "Last Seed", 7 },
        { "Hearthfire", 8 },
        { "Frost Fall", 9 },
        { "Sun's Dusk", 10 },
        { "Evening Star", 11 },
    };

    // TODO: account for other localizations
    static const std::string daySep = " (Day ";

    HtmlParser::HtmlParser(std::string&& contents):
        mContents(contents),
        mCursor(0u)
    {}

    void HtmlParser::skipNextTag()
    {
        if (!done() && mContents.at(mCursor) == '<')
        {
            ++mCursor;

            while (!done() && mContents.at(mCursor) != '>')
                ++mCursor;

            ++mCursor;
        }
    }

    void HtmlParser::skipWhitespace()
    {
        while (!done() && whitespace.count(mContents.at(mCursor)))
            ++mCursor;
    }

    std::string HtmlParser::getNextContents()
    {
        auto start = mCursor;

        while (!done() && mContents.at(mCursor) != '<')
            ++mCursor;

        return mContents.substr(start, mCursor - start);
    }

    bool HtmlParser::done() const
    {
        return mCursor >= mContents.size();
    }

    // Convert a date from a journal entry into numbers
    static void convertDate(std::string_view date, ESM::JournalEntry& entry)
    {
        // Example date: 16 Last Seed (Day 1)

        // Read day of the month
        size_t cursor = 0u;
        while (date.at(cursor) != ' ')
            ++cursor;

        auto dayOfMonthStr = date.substr(0, cursor);
        std::from_chars(dayOfMonthStr.data(), dayOfMonthStr.data() + dayOfMonthStr.size(), entry.mDayOfMonth);

        // move ahead, removing space between day and month
        date.remove_prefix(dayOfMonthStr.size() + 1);

        // Read current month
        cursor = 0u;
        while (date.at(cursor) != '(')
            ++cursor;

        auto monthStr = date.substr(0, cursor - 1);
        entry.mMonth = months.find(monthStr)->second;

        // move ahead to where the day number starts
        date.remove_prefix(monthStr.size() + daySep.size());

        // Read current day (how old the save file is in days)
        cursor = 0u;
        while (date.at(cursor) != ')')
            ++cursor;

        auto dayStr = date.substr(0, cursor);
        std::from_chars(dayStr.data(), dayStr.data() + dayStr.size(), entry.mDay);
    }

    void JOUR::load(ESM::ESMReader& esm)
    {
        HtmlParser parser(esm.getHNString("NAME"));

        while (!parser.done())
        {
            // Create new journal entry
            auto& entry = mEntries.emplace_back();
            entry.mType = ESM::JournalEntry::Type_Journal;

            // Extract journal entry's date and text
            parser.skipNextTag();
            auto date = parser.getNextContents();
            parser.skipNextTag();
            parser.skipNextTag();
            parser.skipWhitespace();
            entry.mText = parser.getNextContents();
            parser.skipNextTag();
            parser.skipWhitespace();

            // Extract date numbers from date text
            convertDate(date, entry);
        }
    }

}
