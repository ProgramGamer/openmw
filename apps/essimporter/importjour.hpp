#ifndef OPENMW_ESSIMPORT_IMPORTJOUR_H
#define OPENMW_ESSIMPORT_IMPORTJOUR_H

#include <string>
#include <vector>

#include <components/esm3/journalentry.hpp>

namespace ESM
{
    class ESMReader;
}

namespace ESSImport
{

    /// HTML parser for journal text
    class HtmlParser
    {
    public:
        // Create a parser containing a string
        HtmlParser(std::string&& contents);
        
        // Skip the next HTML tag
        void skipNextTag();
        
        // Skip all upcoming whitespace
        void skipWhitespace();
        
        // Read contents of a tag into a string and returns it
        std::string getNextContents();
        
        // Checks whether the parser has reached the end of its string
        bool done() const;

    private:
        // string contents of the parser
        std::string mContents;
        
        // current position into the parser's string
        size_t mCursor;
    };

    /// Journal
    struct JOUR
    {
        std::vector<ESM::JournalEntry> mEntries;

        void load(ESM::ESMReader& esm);
    };

}

#endif
